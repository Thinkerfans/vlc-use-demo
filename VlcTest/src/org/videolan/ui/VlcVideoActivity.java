package org.videolan.ui;

import java.io.File;

import org.videolan.libvlc.EventHandler;
import org.videolan.libvlc.IVideoPlayer;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.LibVlcException;
import org.videolan.vlc.util.Util;
import org.videolan.vlc.util.VLCInstance;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.example.vlctest.R;

public class VlcVideoActivity extends Activity implements
		SurfaceHolder.Callback, IVideoPlayer, OnClickListener {

	public final static String PATH = "path";

	private final static String TAG = "[VlcVideoActivity]";

	private SurfaceView mSurfaceView;
	private LibVLC mLibVLC;

	private SurfaceHolder mSurfaceHolder;

	private View mLoadingView, mControlView;

	private int mVideoHeight;
	private int mVideoWidth;
	private int mVideoVisibleHeight;
	private int mVideoVisibleWidth;
	private int mSarNum;
	private int mSarDen;

	private ImageButton mPlayPauseBt;
	private SeekBar mSeekbar;
	private TextView mCurTimeTv, mTotleTimeTv, mTitleTv;
	private String mPath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_video_vlc);

		initViews();
		initListener();
		initVlcLib();

	}

	private void initVlcLib() {
		Intent i = getIntent();
		String path = null;
		if (i != null && (path = i.getStringExtra(PATH)) != null) {
		} else {
			path = "/mnt/sdcard/IPcamer/video/PPCN.avi";

		}

		try {
			mLibVLC = VLCInstance.getLibVlcInstance();
		} catch (LibVlcException e) {
			e.printStackTrace();
		}
		mLibVLC.eventVideoPlayerActivityCreated(true);

		EventHandler em = EventHandler.getInstance();
		em.addHandler(mVlcHandler);

		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mSurfaceView.setKeepScreenOn(true);
		mPath = LibVLC.PathToURI(path);
		mLibVLC.playMRL(mPath);
	}

	private void initListener() {
		
		mSeekbar.setOnSeekBarChangeListener(mSeekListener);
		mPlayPauseBt.setOnClickListener(this);
	}

	private void initViews() {
		mSurfaceView = (SurfaceView) findViewById(R.id.video);
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.setFormat(PixelFormat.RGBX_8888);
		mSurfaceHolder.addCallback(this);

		mLoadingView = findViewById(R.id.video_loading);

		mControlView = findViewById(R.id.control_layout);
		mControlView.setVisibility(View.INVISIBLE);
		mSeekbar = (SeekBar) mControlView
				.findViewById(R.id.mediacontroller_seekbar);
		mPlayPauseBt = (ImageButton) mControlView
				.findViewById(R.id.mediacontroller_play_pause);
		mTitleTv = (TextView) mControlView
				.findViewById(R.id.mediacontroller_file_name);
		mTotleTimeTv = (TextView) mControlView
				.findViewById(R.id.mediacontroller_time_total);
		mCurTimeTv = (TextView) mControlView
				.findViewById(R.id.mediacontroller_time_current);

	}

	@Override
	public void onPause() {
		super.onPause();

		if (mLibVLC != null) {
			mLibVLC.stop();
			mSurfaceView.setKeepScreenOn(false);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mLibVLC != null) {
			mLibVLC.eventVideoPlayerActivityCreated(false);

			EventHandler em = EventHandler.getInstance();
			em.removeHandler(mVlcHandler);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		setSurfaceSize(mVideoWidth, mVideoHeight, mVideoVisibleWidth,
				mVideoVisibleHeight, mSarNum, mSarDen);
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (mLibVLC != null) {
			mSurfaceHolder = holder;
			mLibVLC.attachSurface(holder.getSurface(), this);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		mSurfaceHolder = holder;
		if (mLibVLC != null) {
			mLibVLC.attachSurface(holder.getSurface(), this);// , width, height
		}
		if (width > 0) {
			mVideoHeight = height;
			mVideoWidth = width;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mLibVLC != null) {
			mLibVLC.detachSurface();
		}
	}

	@Override
	public void setSurfaceSize(int width, int height, int visible_width,
			int visible_height, int sar_num, int sar_den) {
		mVideoHeight = height;
		mVideoWidth = width;
		mVideoVisibleHeight = visible_height;
		mVideoVisibleWidth = visible_width;
		mSarNum = sar_num;
		mSarDen = sar_den;
		mHandler.removeMessages(HANDLER_SURFACE_SIZE);
		mHandler.sendEmptyMessage(HANDLER_SURFACE_SIZE);
	}

	private static final int HANDLER_BUFFER_START = 1;
	private static final int HANDLER_BUFFER_END = 2;
	private static final int HANDLER_SURFACE_SIZE = 3;

	private static final int SURFACE_BEST_FIT = 0;
	private static final int SURFACE_FIT_HORIZONTAL = 1;
	private static final int SURFACE_FIT_VERTICAL = 2;
	private static final int SURFACE_FILL = 3;
	private static final int SURFACE_16_9 = 4;
	private static final int SURFACE_4_3 = 5;
	private static final int SURFACE_ORIGINAL = 6;
	private int mCurrentSize = SURFACE_BEST_FIT;

	private Handler mVlcHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg == null || msg.getData() == null)
				return;

			switch (msg.getData().getInt("event")) {
			case EventHandler.MediaPlayerTimeChanged:
				setOverlayProgress();
				break;
			case EventHandler.MediaPlayerPositionChanged:
				break;
			case EventHandler.MediaPlayerPlaying:
				Log.e(TAG, " MediaPlayerPlaying  : ");

				break;
			case EventHandler.MediaPlayerBuffering:
				Log.e(TAG, " MediaPlayerPlaying  : ");
				break;
			case EventHandler.MediaPlayerLengthChanged:
				break;
			case EventHandler.MediaPlayerVout:
				Log.e(TAG, " MediaPlayerVout  : ");
				mHandler.removeMessages(HANDLER_BUFFER_END);
				mHandler.sendEmptyMessage(HANDLER_BUFFER_END);

				initVideoInfo();
				mLibVLC.setTime(0);
				break;
			case EventHandler.MediaPlayerEndReached:
				// 播放完成
				Util.toaster(VlcVideoActivity.this, R.string.app_name);
				finish();
				break;
			case EventHandler.MediaPlayerEncounteredError:
				encounteredError();
				break;
			default:
				break;
			}

		}

	};

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HANDLER_BUFFER_START:
				showLoading(true);
				break;
			case HANDLER_BUFFER_END:
				showLoading(false);
				mControlView.setVisibility(View.VISIBLE);
				break;
			case HANDLER_SURFACE_SIZE:
				changeSurfaceSize();
				break;
			}
		}
	};

	private void showLoading(boolean isVisiable) {
		if (isVisiable) {
			mLoadingView.setVisibility(View.VISIBLE);
		} else {
			mLoadingView.setVisibility(View.GONE);
		}

	}

	private void initVideoInfo() {

		long len = mLibVLC.getLength();
		mTotleTimeTv.setText(Util.millisToString(len));
		String title = mPath;
		if (mPath.startsWith("file:")) {
			title = new File(mPath).getName();
		}
		mTitleTv.setText(title);

		int length = (int) mLibVLC.getLength();
		mSeekbar.setMax(length);

	}

	private void changeSurfaceSize() {
		// get screen size
		int dw = getWindowManager().getDefaultDisplay().getWidth();
		int dh = getWindowManager().getDefaultDisplay().getHeight();

		// calculate aspect ratio
		double ar = (double) mVideoWidth / (double) mVideoHeight;
		// calculate display aspect ratio
		double dar = (double) dw / (double) dh;

		switch (mCurrentSize) {
		case SURFACE_BEST_FIT:
			if (dar < ar)
				dh = (int) (dw / ar);
			else
				dw = (int) (dh * ar);
			break;
		case SURFACE_FIT_HORIZONTAL:
			dh = (int) (dw / ar);
			break;
		case SURFACE_FIT_VERTICAL:
			dw = (int) (dh * ar);
			break;
		case SURFACE_FILL:
			break;
		case SURFACE_16_9:
			ar = 16.0 / 9.0;
			if (dar < ar)
				dh = (int) (dw / ar);
			else
				dw = (int) (dh * ar);
			break;
		case SURFACE_4_3:
			ar = 4.0 / 3.0;
			if (dar < ar)
				dh = (int) (dw / ar);
			else
				dw = (int) (dh * ar);
			break;
		case SURFACE_ORIGINAL:
			dh = mVideoHeight;
			dw = mVideoWidth;
			break;
		}

		mSurfaceHolder.setFixedSize(mVideoWidth, mVideoHeight);
		ViewGroup.LayoutParams lp = mSurfaceView.getLayoutParams();
		lp.width = dw;
		lp.height = dh;
		mSurfaceView.setLayoutParams(lp);
		mSurfaceView.invalidate();
	}

	/**
	 * update the overlay
	 */
	private int setOverlayProgress() {
		if (mLibVLC == null) {
			return 0;
		}
		int time = (int) mLibVLC.getTime();
		mSeekbar.setProgress(time);
		mCurTimeTv.setText(Util.millisToString(time));
		return time;
	}

	private void encounteredError() {
		/* Encountered Error, exit player with a message */
		AlertDialog dialog = new AlertDialog.Builder(this)
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						}).setTitle(R.string.encountered_error_title)
				.setMessage(R.string.encountered_error_message).create();
		dialog.show();
	}

	/**
	 * handle changes of the seekbar (slicer)
	 */
	private  OnSeekBarChangeListener mSeekListener = new OnSeekBarChangeListener() {

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// mDragging = true;
			// showOverlay(OVERLAY_INFINITE);
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// mDragging = false;
			// showOverlay();
			// hideInfo();
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			if (fromUser) {
				if(mLibVLC != null){
					mLibVLC.setTime(progress);
					setOverlayProgress();
					mCurTimeTv.setText(Util.millisToString(progress));
				}
			}

		}
	};

	@Override
	public void onClick(View v) {
//		if (v.getId() == R.id.back) {
//			if (mLibVLC != null) {
//					mLibVLC.stop();
//			}
//			finish();
//		} else {
			if (mLibVLC != null) {
				if (mLibVLC.isPlaying()) {
					mLibVLC.pause();
					mPlayPauseBt
							.setImageResource(R.drawable.mediacontroller_play);
				} else {
					mLibVLC.play();
					mPlayPauseBt
							.setImageResource(R.drawable.mediacontroller_pause);
				}

			}
//		}

	}
}
